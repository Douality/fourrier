#include "Fourrier.cpp"
#include "FastFourrier.h"
#include "EvalPerf.h"
#include <iostream>
#define epsilon 0.0001
#define fLa 440.0
#define fechan 44500.0
#define t 6.0
using namespace std;
/*
 astuces pour nul :
 prog >> fichier pour stocker
 grep -e "vafind" "where"
 alias :
    -alias comp="clear;g++ -c -Wall main.cpp";
    -alias obj="comp;g++ -o main main.o";
    -alias exec="obj;clear;./main BrokenGlass.wav BrokenGlassFourrier.wav 440.0 44500.0 6"
*/

int main(int argc, char** argv){

  if (argc!=5){
    std::cout<<"./extract fileIn fileOut Hz temps"<<std::endl;
    return 0;
  }

  Wave la;
  Fourrier fourrier;
  EvalPerf Eval;

  la.read(argv[1]);
    
  double inputf = atof(argv[3]); //filtre
  //double Mi = 82.84; //note
  //double Do = 261.6; //note
  //double Re = 290.3; //note
  //double laFreq = 440.0; //note
  //double Fa = 639.39; //note
  //double Sol = 741.4; //note
  //double Si = 993.1; //note

  double fe = 44100.0;
  int nbechantillons;
  double temps = 3.0;
  temps = atof(argv[4]);
  double* realdata;
  unsigned char* data8;
  short mono = 1;

  nbechantillons = (int)std::floor(temps*fe);
  realdata = new double[nbechantillons];

  data8 = new unsigned char [nbechantillons];

  //fourrier.Creation_note(inputf, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Re, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Do, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Mi, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Fa, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Sol, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(Si, fe, nbechantillons, realdata); //note
  //fourrier.Creation_note(laFreq, fe, nbechantillons, realdata); //note

  
  la.getData8(&data8,&nbechantillons);
  std::cout<<nbechantillons<<endl;
  //double signal[nbechantillons]; // save
  double transform[nbechantillons]; //->butterworth
  double real[nbechantillons]; //->butterworth
  double imaginaire[nbechantillons]; //->butterworth

  for(int i = 0; i < nbechantillons; i++){
    realdata[i] = fourrier.convert_double(data8[i]);
    //signal[i] = realdata[i]; //save ou ->butterworth
    transform[i]=0.; //->butterworth
  }

  //FFT(1,nbechantillons,signal,realdata); //inutile
  fourrier.DFT(realdata,real,imaginaire,nbechantillons); //->butterworth

  for(int i=0;i<nbechantillons;i++){
    //std::cout<<"signal="<<signal[i]<<"  |&&|   fourrier="<<transform[i]<<std::endl;
  }

  //fourrier.passe_bas(realdata,nbechantillons,inputf); //-> passe_bas
  //fourrier.passe_haut(realdata,nbechantillons,inputf); //-> passe_haut
  //fourrier.filtreButterworth(imaginaire,transform,nbechantillons,fe,inputf); //->butterworth
  fourrier.filtreButterworth(real,transform,nbechantillons,fe,inputf); //->butterworth
  
  /*for(int i = 0; i < nbechantillons; i++){
    transform[i]=255-transform[i];
  }*/

  //FFT(-1,nbechantillons,transform,signal); //inutile
  fourrier.IDFT(realdata,transform,imaginaire,nbechantillons); //->butterworth

  std::cout<<"------------------------------------------------"<<std::endl;
  for(int i = 0; i < nbechantillons; i++){
    //data8[i] = fourrier.convert_char(realdata[i]);  // tout
    data8[i] = fourrier.convert_char(transform[i]);
    //std::cout<<"filtre = "<<(signal[i]+1.0)*127.5<<"Hz"<<std::endl;
  }

  Wave la2(data8,nbechantillons,mono,fe); //tout

  la.modifData8(data8); //tout
  la2.write(argv[2]); //tout

  //Eval.affiche(nbechantillons*nbechantillons*nbechantillons);

  //char exit[100] = "La.wav";
  //to = 1/44100 (1/fe)
  //T = 1/440 (1/fla)
  //k: tk = k*to
  //S(t) = sin(2*pi*fla*t)
  //Hk = sin(2*pi*fla*kto)
  //   = sin(2*pi*(fla/fe)*k)     
  //6s -> nbechantillons * to = 6s
  delete[] realdata;

	return 0;
}

//Eval -> performance, peut rester en //
//En bas -> c pour des tests, inutiles peut rester en //
// les // sont a déquote pour les exercices
  // //butterwoth sont a décoter pour faire le butterwooth mais faut quote tout les non butterworth
  // pareil pour //passe_bas, note ,...
  // //tout faut pas déquote
  // //inutile ne sert plus à rien
