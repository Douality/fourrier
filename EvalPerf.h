#include<iostream>
#include<ctime>
#include<chrono>
#include<time.h>
#include<string>


class EvalPerf{
private:
  //std::chrono::time_point<std::chrono::high_resolution_clock> now;
  //std::chrono::time_point<std::chrono::high_resolution_clock> end;
  std::chrono::high_resolution_clock::time_point now;
  std::chrono::high_resolution_clock::time_point end;
  double st_cycles;
  double ed_cycles;
  double nb_instructions;
public:
  EvalPerf() {
    this->now=std::chrono::high_resolution_clock::now();
    this->end=std::chrono::high_resolution_clock::now();
    this->st_cycles=0;
    this->ed_cycles=0;
    this->nb_instructions=0;
  };
  EvalPerf(double nb) {
    this->now=std::chrono::high_resolution_clock::now();
    this->end=std::chrono::high_resolution_clock::now();
    this->st_cycles=0;
    this->ed_cycles=0;
    this->nb_instructions=nb;
  };
  EvalPerf(double s,double e,double nb) {
    this->now=std::chrono::high_resolution_clock::now();
    this->end=std::chrono::high_resolution_clock::now();
    this->st_cycles=s;
    this->ed_cycles=e;
    this->nb_instructions=nb;
  };
  uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
  };
  void start(){
    this->now=std::chrono::high_resolution_clock::now();
    this->st_cycles=(double)this->rdtsc();
  }
  void stop(){
    this->end=std::chrono::high_resolution_clock::now();
    this->ed_cycles=(double)this->rdtsc();
  };
  double nb_cycle(){
    return (this->ed_cycles)-(this->st_cycles);
  }
  double second(){
    //double r=(double)(std::chrono::duration_cast<std::chrono::seconds>((this->now)-(this->end)).count());
    //std::cout<< "double second() return "<<r<<std::endl;
    std::chrono::duration<double> fs = end-now;
    std::chrono::seconds d = std::chrono::duration_cast< std::chrono::seconds >( fs );
    return (double)d.count();
  };
  double millisecond(){
    std::chrono::duration<double> fs = end-now;
    std::chrono::milliseconds d = std::chrono::duration_cast< std::chrono::milliseconds >( fs );
    return (double)d.count();
  };
  double ipc(){
    return ((double)(this->nb_instructions))/(this->nb_cycle());
  };
  double cpi(){
    return (double)(1/(this->ipc()));
  };
  double ipc(double n){
    return n/(this->nb_cycle());
  };
  double cpi(double n){
    return 1/(this->ipc(n));
  };
  void affiche(){
    std::cout <<"nbr cycles: "<<this->nb_cycle()<<std::endl;
    std::cout <<"nbr secondes: "<<this->second()<<"s"<<std::endl;
    std::cout <<"nbr millisecondes: "<<this->millisecond()<<"ms"<<std::endl;
    std::cout <<"nbr CPI=: "<<this->cpi()<<std::endl;
  }
  void affiche(int n){
    std::cout <<"nbr cycles: "<<this->nb_cycle()<<std::endl;
    std::cout <<"nbr secondes: "<<this->second()<<"s"<<std::endl;
    std::cout <<"nbr millisecondes: "<<this->millisecond()<<"ms"<<std::endl;
    std::cout <<"nbr CPI=: "<<this->cpi(n)<<std::endl;
  }
};
