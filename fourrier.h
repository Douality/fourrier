#ifndef FOURRIER_H
#define FOURRIER_H

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <cassert>

#include "wave.cpp"

struct Complexe{
    double real;
    double Img;
    Complexe(){
        real=0;
        Img=0;
    }
    Complexe(double a, double b){
        real=a;
        Img=b;
    }
};

class Fourrier{

    public :

        double fe;
        double fl;
        double fq;

        Fourrier();
        Fourrier(double fnote,double fechantillon);

        double convert_double(unsigned char e);
        unsigned char convert_char(double x);
        void full_convert_double(double* res,unsigned char* e,size_t n);
        void full_convert_char(unsigned char* res,double* e,size_t n);

        void Creation_note(double f,double fr,int nb,double* data_reel);
        void Creation_note2(double fq,int nb,double* data_reel);

        double s(double t);
        double x(double k);
        int get_pow(double x);
        void DFT(double *signal, double *partie_reelle, double *partie_imaginaire, int N);
        void IDFT(double *signal, double *partie_reelle, double *partie_imaginaire, int N);

        double get_h(int taille, double fc, double Hertz);
        double get_k(int taille, double fc, double Hertz);
        int get_indice_k(double* data, int taille, double fc, double fech);
        double get_alpha(double cut, double fech);

        void passe_bas(double* data, int taille, double fc, double fech);
        void passe_haut(double* data, int taille, double fc, double fech);
        void passe_bas(double* data, int taille, double fc);
        void passe_haut(double* data, int taille, double fc);
        void filtreButterworth(double *signal, double *signal_filtre, int N, double freq_ech, double freq_cut);

        void display(unsigned char* data,size_t N);
        void display(double* data,size_t N);
};
#endif