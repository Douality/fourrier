# Projet de Transformation de Fourier en C++ pour l'Analyse de Sons

Ce projet consiste en une implémentation en C++ de la transformation de Fourier pour l'analyse de fichiers audio. L'objectif principal est de fournir une application permettant de convertir des signaux audio en données fréquentielles, afin de fournir une analyse des spectres sonores dans des logiciels tels qu'Audacity.

## Fonctionnalités

- **Transformation de Fourier Discrète (DFT)** : L'application prend en charge la DFT pour convertir un signal temporel en son équivalent en fréquences.
- **Formats de fichiers audio supportés** : Vous pouvez charger des fichiers audio aux formats courants tels que WAV, MP3, etc.
- **Exportation de données** : Les données fréquentielles peuvent être exportées sous forme de fichiers (.wav notanment) pour une analyse plus poussée dans des outils comme Audacity.
- **Utilisation** : L'application dispose d'une interface utilisateur simple pour charger des fichiers audio, donner des paramètres, effectuer la transformation et exporter les résultats.

## Utilisation

1. Clonez ce référentiel sur votre système local.
2. Compilez l'application en utilisant un compilateur C++ compatible (g++, clang++, etc.).
3. Exécutez l'application en spécifiant le fichier audio en entrée et les options de transformation.
4. Les résultats peuvent être exportés au format CSV ou tout autre format compatible avec Audacity.
5. Chargez les données exportées dans Audacity pour une analyse graphique approfondie du spectre sonore.

0. Certains fichier.wav sont fournis pour test das le main folder et certaines captures dans le result folder.
0. A noter que ce code n'est pas forcément propres et que certaines modification devons être faites directement dans le main.cpp, les résultats sont à prendre en conséquence. 

## Dépendances

Ce projet utilise les bibliothèques C++ standard pour la manipulation de fichiers audio. Vous devrez peut-être installer certaines dépendances, notamment des bibliothèques de lecture de fichiers audio, en fonction de votre système d'exploitation.
Cependant la majorité des fonctions de bases pour les transformées de Fourrier et quelques analyses sont incluses.

## Améliorations possibles

- Ajouter la prise en charge de formats audio supplémentaires.
- Implémenter des algorithmes de transformation plus rapides, tels que la FFT (Fast Fourier Transform).
- Créer une interface utilisateur plus avancée.
- Gérer les erreurs et les exceptions de manière plus robuste.
- Ajouter des fonctionnalités d'analyse avancée, telles que la détection de fréquences dominantes.

