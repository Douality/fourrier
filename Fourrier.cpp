#include "fourrier.h"
using namespace std;

Fourrier::Fourrier(){
    fe=0.;
    fl=0.;
    fq=0.;
}

Fourrier::Fourrier(double fnote,double fechantillon){
    fl=fnote;
    fe=fechantillon;
    fq=fl/fe;
    assert(fe>=2*fl);
}

double Fourrier::convert_double(unsigned char e){
    double reel;
    reel = (double)e/127.5;
    return reel - 1.0;
}

unsigned char Fourrier::convert_char(double x){
    //return (unsigned char) std::floor((xn-1.0)*127.5);
    x = (x+1.0)*127.5;
    x = x < 0.0 ? 0.0 : x;
    x = x > 255.0 ? 255.0 : x;
    return (unsigned char)std::floor(x);
}

void Fourrier::full_convert_double(double* res,unsigned char* e,size_t n){
    for(size_t i=0;i<n;i++){
        res[i]=Fourrier::convert_double(e[i]);
    }
}

void Fourrier::full_convert_char(unsigned char* res,double* e,size_t n){
    for(size_t i=0;i<n;i++){
        res[i]=Fourrier::convert_char(e[i]);
    }
}

int Fourrier::get_pow(double x){
    int N=(int)(x);
    double p = log(N)/log(2);
    return (int)(ceil(p)+1);
}

double Fourrier::get_h(int taille, double fr, double fech){
    double k=0.;
    int counter=0;
    double div=fr/taille;
    while(counter<taille && k*div > fech) {
        ++counter;
        ++k;
    }
    return k;
}

double Fourrier::get_k(int taille, double fc, double fech){
    return (fc*(double)(taille)/fech)/2;
}

int Fourrier::get_indice_k(double* data,int taille, double fc, double fech){
    double k = Fourrier::get_k(taille,fc,fech);
    k = (k/127.5)-1.0;
    std::cout<<"Frag_cut = "<<k<<" Hz "<<std::endl;
    int i=0;
    for(;i<taille && k>((data[i]*taille)/fech);++i){;}
    std::cout<<"T["<<i<<"]"<<std::endl;
    return i;
}

void Fourrier::passe_bas(double* data, int taille, double fc){
    double reel = fc/127.5;
    reel -= 1.0;
    for(int i=0;i<taille;++i){
        data[i]=data[i]>reel? 0. : data[i];
    }
}

void Fourrier::passe_haut(double* data, int taille, double fc){
    double reel = fc/127.5;
    reel -= 1.0;
    for(int i=0;i<taille;++i){
        data[i]=data[i]>reel? data[i] : 0.;
    }
}

void Fourrier::passe_bas(double* data, int taille, double fc, double fech){
    int i=0;
    int cut = Fourrier::get_indice_k(data,taille,fc,fech);
    for(;i<cut;++i){;}
    for(;i<taille-cut;++i) {
        data[i]=0.;
    }
    for(;i<taille;++i){;}
}

void Fourrier::passe_haut(double* data, int taille, double fc, double fech){
    int i=0;
    std::cout<<"Taille passe_haut = "<<taille<<std::endl;
    int cut = Fourrier::get_indice_k(data,taille,fc,fech);
    for(;i<cut;++i){
        data[i]=0.;
    }
    for(;i<taille-cut;++i) {;}
    for(;i<taille;++i){
        std::cout<<"indice avant bug i : "<<i<<std::endl;
        data[i]=0.;
    }
}

void Fourrier::Creation_note(double f,double fr,int nb,double* data_reel){
    double alpha;
    int k;
    double fq=f/fr;
    alpha=2.0*M_PI*fq;
    for(k=0;k<nb;k++){
        data_reel[k]=sin(((double)(k))*alpha);
    }
}
      
void Fourrier::Creation_note2(double fq,int nb,double* data_reel){
    double alpha,beta;
    int k;
    alpha=2.0*M_PI*fq;
    beta=0.1*alpha;
    for(k=0;k<nb;k++){
        data_reel[k]=sin(((double)(k))*(k/nb)*alpha + (1.0 - ((k/nb)*beta)));
    }
}

double Fourrier::s(double t){
    return sin(2*M_PI*fl*t);
}

double Fourrier::x(double k){
    return sin(2*M_PI*(fl/fe)*k);
}

void Fourrier::DFT(double* signal, double* partie_reelle, double* partie_imaginaire, int N){
  double reel;
  double imaginaire;
  double pin = 2.0*M_PI/(double)N;
  for(int i=0;i<N;i++){
      /*if(i%100==0){
          std::cout<<"process: "<< i << "/" << data_nb <<std::endl;
      }*/
      double i2pi = (double)i*pin;
    reel=0.0;
    imaginaire=0.0;
    for(int k=0;k<N;k++){
        double trig = (double)k*i2pi;
        reel += cos(trig) * signal[k];
        imaginaire -= sin(trig) * signal[k];
    }
    partie_reelle[i] = reel;
    partie_imaginaire[i] = imaginaire;
  }
}

void Fourrier::IDFT(double* signal, double* partie_reelle, double* partie_imaginaire, int N){
    double pin = 2.0*M_PI/(double)N;
    for(int i=0;i<N;i++){
        signal[i] = 0;
        double i2pi = (double)i*pin;
        for(int k = 0; k < N; k++){
            double phase = (double)k*i2pi;
            signal[i] += cos(phase) * partie_reelle[k] - sin(phase) * partie_imaginaire[k];
        }
        signal[i] /= (double)N;
    }
}

void Fourrier::display(unsigned char* data,size_t N){
    for(size_t i=0;i<N;i++){
        std::cout<<data[i]<<std::endl;
    }
}

void Fourrier::display(double* data, size_t N){
    for(size_t i=0;i<N;i++){
        std::cout<<data[i]<<std::endl;
    }
}

double get_alpha(double cut, double fech){
    return M_PI*cut/fech;
}

void Fourrier::filtreButterworth(double *signal, double *signal_filtre, int N, double freq_ech, double freq_cut){
  
  double alpha = M_PI * freq_cut/freq_ech;

  double A,B,C,D;
  double s1 = 2 * alpha;
  double s2 = 2 * pow(alpha,2);
  double s3 = pow(alpha,3);
  A = (1+s1+s2+s3);
  B = (-3-s1+s2+3*s3);
  C = (3-s1-s2+3*s3);
  D = (-1+s1-s2+s3);

  double a[4], b[4];
  b[0] = alpha * alpha * alpha;
  b[3] = b[0];
  b[1] = 3.0*b[0];
  b[2] = b[1];

  a[0] = 0.0;
  a[1] = -B / A;
  a[2] = -C / A;
  a[3] = -D / A;
  
  int n, k, m;
  for(n = 0; n < N; n++){
    signal_filtre[n] = 0.0;
    for(k = 0; k < 4; k++){
      m = n-k;
      if(m>0){
        signal_filtre[n] += b[k] * signal[m];
        signal_filtre[n] += a[k] * signal_filtre[m];
      }
    }
  }

  

}
